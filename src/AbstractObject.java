// AbstractObject.java
// Jim Sproch
// Created: April 29, 2006
// Modified: January 6, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	AbstractObjects are objects that can not be seen or moved!
	@author Jim Sproch
	@version 0.1a beta
*/


import java.util.*;

public interface AbstractObject
{
	public Location getlocation();
	public Size getsize();
	public String toString();
	public AbstractObject getOwner();
	public ArrayList<Boundary> getBoundaries();
	public void destroy();
	public Status getStatus();
	public int getTeam();
	public void setTeam(int team);
}