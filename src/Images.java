// Images.java
// Jim Sproch
// Created: April 29, 2006
// Modified: February 20, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Images holds images for use by other objects!
	@author Jim Sproch
	@version 0.1a beta
*/


import java.awt.*;
import javax.swing.*;
import java.util.*;  //for arraylist

import java.net.URL; //URLs



public class Images
{

	private static HashMap<String, Picture> pictures = new HashMap<String, Picture>();


	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	public static void loadimages()
	{
		loadImage(new Picture(), Direction.NULL, "block_25x25_0x0_black");
		loadImage(new Picture(), Direction.NULL, "block_25x25_0x0_green");
		loadImage(new Picture(), Direction.NULL, "bullet_4x4_1x1_black");
		loadImage(new Picture(), Direction.NULL, "bullet_4x4_1x1_green");
		loadImage(new Picture(), Direction.NULL, "bullet_4x4_1x1_grey");
		loadImage(new Picture(), Direction.NULL, "bullet_4x4_1x1_lightblue");
		loadImage(new Picture(), Direction.NULL, "bullet_4x4_1x1_yellow");
		loadImage(new Picture(), Direction.NULL, "RemoteDetonator");
		loadImage(new Picture(), Direction.NULL, "TimeBomb");
		loadImage(new Picture(), Direction.NULL, "mine");
		loadImageInAllDirections("ship_23x23_2x2_red");
		loadImageInAllDirections("ship_23x23_2x2_green");
		loadImageInAllDirections("ship_23x23_2x2_grey");

		try{ Thread.sleep(2000); }catch(Exception e){}
	}


	// Some day, these next two methods will be replaced with auto-searching methods
	public static void loadImage(Picture picture, int direction, String imageName)
	{
		String path = "Images/"+imageName;
		if(direction != Direction.NULL) path = path+"-"+Direction.toString(direction)+".png";
			else path = path+".png";
		URL fullpath = Images.class.getResource(path);
		Image tempimg = (new ImageIcon(fullpath)).getImage();
		picture.putImage(direction, tempimg);
		pictures.put(imageName, picture);
	}

	public static void loadImageInAllDirections(String imageName)
	{
		Picture picture = new Picture();
		loadImage(picture, Direction.NORTH, imageName);
		loadImage(picture, Direction.SOUTH, imageName);
		loadImage(picture, Direction.EAST, imageName);
		loadImage(picture, Direction.WEST, imageName);
	}

	public static Picture getPicture(String key)
	{
		return pictures.get(key);
	}
}