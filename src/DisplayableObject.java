// DisplayableObject.java
// Jim Sproch
// Created: April 29, 2006
// Modified: January 6, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	DisplayableObjects are objects that can be seen but not moved!
	@author Jim Sproch
	@version 0.1a beta
*/


import java.awt.*;

public interface DisplayableObject extends AbstractObject
{
	public void paint(Graphics g);
	public boolean isDestroyable();
}