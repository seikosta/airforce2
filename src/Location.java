// Location.java
// Jim Sproch
// Created: April 29, 2006
// Modified: May 13, 2007
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Locations define where an object is given an x and y coord!
	@author Jim Sproch
	@version 0.1a beta
*/


public class Location
{
	private int myx;
	private int myy;

	public Location(int x, int y)
	{
		myx = x;
		myy = y;
	}

	public int getx()
	{
		return myx;
	}
	public int gety()
	{
		return myy;
	}

	public void setx(int x)
	{
		myx = x;
	}
	public void sety(int y)
	{
		myy = y;
	}
	


	public String toString()
	{
		return "Location: ("+myx+", "+myy+")";
	}

	public void move(int direction)
	{
		if(direction == 12) myy--;
		if(direction == 6) myy++;
		if(direction == 9) myx--;
		if(direction == 3) myx++;
	}


	public static void main(String[] args)
	{
		Printer.noexecute();
	}

}