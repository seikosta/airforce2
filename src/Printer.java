// Printer.java
// Jim Sproch
// Created: April 29, 2006
// Modified: January 10, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

import java.io.*;

/**
	Printer prints debugging output and the intro text for an executed class!
	@author Jim Sproch
	@version 0.1a beta
*/

public class Printer extends PrintStream
{
	// Hold the stream information used by the System (in case we want to re-apply it)
	public static PrintStream stdout = System.out;
	public static PrintStream stderr = System.err;

	// Public instances of Printer to be invoked from a static context
	public static Printer debug = new Printer(Printer.stdout);	// Debug - typically developers use to solve specific problems
	public static Printer out = new Printer(null);		// Out - This could be anything that doesn't fit elsewhere
	public static Printer wrn = new Printer(Printer.stdout);// Warnings - the user may or may not want to see
	public static Printer xml = new Printer(null);		// XML parsing data (useful when working with the parser)
	public static Printer err = new Printer(Printer.stderr);// Typically near-fatal errors, leave this on when developing
	public static Printer note = new Printer(Printer.stdout);// Typically user-visible comments
	public static Printer info = new Printer(Printer.stderr);// Kinda cool info about what the game is actually doing
	public static Printer fps = new Printer(null);		// FPS Benchmarking - all others should be off when benchmarking

	// When benchmarking, also remember to turn off
	//    * All other printers (Printer.*)
	//    * Ship Acceleration
	//    * Bullet Acceleration
	//    * Level-Based Weapons Use Frequency

	// Constructor
	Printer(PrintStream stream)
	{
		// Create the stream with a dummy print stream.
		super(new PipedOutputStream());

		// If the stream we got is valid, replace the dummy stream, otherwise printer is off (null passed)
		if(stream != null) super.out = stream;
	}
	
	
	// Get and Set State of printers and attributes
	public static boolean getState(String element)
	{
		if(element == null) return false;
		if(element.equals("debug_crash")) return false;
		if(element.equals("bugmaker_random")) return false;
		if(element.equals("railgunmode")) return false;
		Printer.err.println("ERROR 421: State could not be determined #Printer.getState()");
		return false;
	}
	// TODO: Create a static setstate function
	//public static void setState(String element, boolean state) {  }

	@Deprecated
	public static int scale()
	{
		return 1;
	}


	// Main: call noexecute()
	public static void main(String[] args)
	{
		noexecute();
	}

	// Print the intro and version number
	public static void printIntro()
	{	
		Printer.stdout.println("*********************************************************");
		Printer.stdout.println("*********************************************************");
		Printer.stdout.println("*********************************************************");
		Printer.stdout.println("**              Mac < Windows < Linux                  **");
		Printer.stdout.println("**          Document Created by Jim Sproch             **");
		Printer.stdout.println("**   AForce Version: 2.1.078b - Alien Airforce Port!   **");
		Printer.stdout.println("*********************************************************");
		Printer.stdout.println("*********************************************************");
		Printer.stdout.println("*********************************************************");
		Printer.stdout.println("AForce.java by Jim Sproch");
	}

	// Prints a much friendlier message than "Exception in thread "main" java.lang.NoSuchMethodError: main"
	public static void noexecute()
	{
		// Prints a much friendlier message than "Exception in thread "main" java.lang.NoSuchMethodError: main"

		Printer.stderr.println("*********************************************************");
		Printer.stderr.println("**                                                     **");
		Printer.stderr.println("**                     SYSTEM ERROR                    **");
		Printer.stderr.println("**                                                     **");
		Printer.stderr.println("**      THIS CLASS IS NOT DESIGNED TO BE EXECUTED      **");
		Printer.stderr.println("**                    TO PLAY, TYPE:                   **");
		Printer.stderr.println("**                                                     **");
		Printer.stderr.println("**                     java AForce                     **");
		Printer.stderr.println("*********************************************************");

		Printer.stderr.print("\n");

		// What the heck, we might as well start the game for them.
		AForce.main(new String[1]);
	}
}
