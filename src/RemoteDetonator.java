// RemoteDetonator.java
// Jim Sproch
// Created: March 31, 2007
// Modified: May 15, 2007
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	RemoteDetonator explodes when the user activates again!
	@author Jim Sproch
	@version 0.1a beta
*/


import java.awt.*;
import javax.swing.*;
import java.util.*;

public class RemoteDetonator extends JComponent implements DisplayableObject, Weapon
{
	Size mysize;
	Location mylocation;
	int myid;
	int mybullettype;
	int myteam;
	Status mystatus;
	Picture myPicture;


	// Because this object is Serializable because it extends JComponent
	private static final long serialVersionUID = 7526471155622776147L;

	ArrayList<Boundary> boundaries = new ArrayList<Boundary>();

	AForceEnv field;
	AbstractObject myowner;


	public String toString()
	{
		return "Mine @ " + mylocation;
	}

	@Deprecated
	public int getdirection()
	{
		return getDirection();
	}

	public int getDirection()
	{
		return Direction.NULL;
	}

	RemoteDetonator(Size size, Location location)
	{
		mybullettype = 0;
		mysize = size;
		mylocation = location;
		mystatus = new Status(10,1);

		boundaries.add(new Boundary(location, mysize.getoffsetx(), mysize.getoffsety(), Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(location, mysize.getoffsetx(), mysize.gety()+mysize.getoffsety(), Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(location, mysize.getoffsetx(), mysize.getoffsety(), Direction.SOUTH, mysize.gety(), this));
		boundaries.add(new Boundary(location, mysize.getx()+mysize.getoffsety(), mysize.getoffsetx(), Direction.SOUTH, mysize.gety(), this));

		myPicture = Images.getPicture("RemoteDetonator");

	}

	@Deprecated
	public boolean isdestroyable()
	{
		return isDestroyable();
	}

	public boolean isDestroyable()
	{
		return false;
	}

	@Deprecated
	public void setteam(int team)
	{
		setTeam(team);
	}

	@Deprecated
	public int getteam()
	{
		return getTeam();
	}

	@Deprecated
	public Status getstatus()
	{
		return getStatus();
	}

	public void setTeam(int team)
	{
		myteam = team;
	}

	public int getTeam()
	{
		return myteam;
	}

	public Status getStatus()
	{
		return mystatus;
	}


	public Size getsize()
	{
		return mysize;
	}

	public Location getlocation()
	{
		return mylocation;
	}


	public void destroy()
	{
		field.removeObject(this);
	}


	public int getid()
	{
		return myid;
	}

	@Deprecated
	public void setfield(AForceEnv tempfield)
	{
		field = tempfield;
	}

	@Deprecated
	public void setowner(AbstractObject owner)
	{
		myowner = owner;
	}

	@Deprecated
	public AbstractObject getOwner()
	{
		return myowner;
	}

	public void setField(AForceEnv tempfield)
	{
		field = tempfield;
	}

	public void setOwner(AbstractObject owner)
	{
		myowner = owner;
	}

	@Deprecated
	public AbstractObject getowner()
	{
		return myowner;
	}

	public static void main(String[] args)
	{
		Printer.noexecute();
	}


	@Deprecated
	public Boundary relaventboundary(int direction)
	{
		return relaventBoundary(direction);
	}


	@Deprecated
	public ArrayList<Boundary> getboundaries()
	{
		return getBoundaries();
	}

	public Boundary relaventBoundary(int direction)
	{
		if(direction == Direction.NORTH) return boundaries.get(0);
		if(direction == Direction.SOUTH) return boundaries.get(1);
		if(direction == Direction.WEST) return boundaries.get(2);
		if(direction == Direction.EAST) return boundaries.get(3);

		Printer.err.println("ERROR 514: Direction could not be detected! Returning Null! #Ship.relaventboundary()");
		return null;
	}


	public ArrayList<Boundary> getBoundaries()
	{
		return boundaries;
	}


	public void paint(Graphics g)
	{

		if(g == null || getStatus().getHealth() == 0) return;
		g.drawImage(myPicture.getImage(Direction.NULL), mylocation.getx(), mylocation.gety(), this);
	}
}