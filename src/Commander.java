// Commander.java
// Jim Sproch
// Created: May 11, 2007
// Modified: February 21, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Commander commands all the non-user ships!
	@author Jim Sproch
	@version 0.1a beta
*/


import java.util.*;

public class Commander
{
	static int index = 0;
	static Hashtable<Integer, DataNode> dataByInt = new Hashtable<Integer, DataNode>();
	static Hashtable<MovableObject, DataNode> dataByObject = new Hashtable<MovableObject, DataNode>();

	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	public static int register(MovableObject owner)
	{
		index++;

		DataNode data = new DataNode(owner);
		dataByObject.put(owner, data);
		dataByInt.put(new Integer(index), data);

		return index;
	}

	static class WeaponsControl
	{
		public static void register(MovableObject owner, AbstractObject weapon)
		{
			// owner registeration is not yet strictly enforced, so if the object is not, we need to register it
			if(!dataByObject.containsKey(owner)) Commander.register(owner);

			// register the weapon!
			dataByObject.get(owner).weaponUsed(weapon.getClass(), AForce.clicker.getElapsedTime());
		}

		public static boolean canUse(MovableObject owner, Class<?> weapon)
		{
			// owner registeration is not yet strictly enforced, so if the object is not, we need to register it
			if(!dataByObject.containsKey(owner)) Commander.register(owner);

			// Deal with Lasers
			if(weapon == Laser.class)
			{
				if(dataByObject.get(owner).weaponLastUsed(weapon) + 20000 < AForce.clicker.getElapsedTime())
					return true;
				return false;
			}

			// Deal with the Warp drive
			if(weapon == Warp.class)
			{
				if(dataByObject.get(owner).weaponLastUsed(weapon) + 5000 < AForce.clicker.getElapsedTime())
					return true;
				return false;
			}

			return false;
		}
	}

}


class DataNode
{
	long laserused = 0;
	long warpused = 0;

	DataNode(MovableObject owner)
	{

	}

	public void weaponUsed(Class<?> weapon, long time)
	{
		if(weapon == Laser.class) laserused = time;
		if(weapon == Warp.class) warpused = time;
	}

	public long weaponLastUsed(Class<?> weapon)
	{
		if(weapon == Laser.class) return laserused;
		if(weapon == Warp.class) return warpused;
		return 0;
	}
}
