// UserIO.java
// Jim Sproch
// Created: April 29, 2006
// Modified: February 20, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	UserIO gets the users key strokes and acts upon them!
	@author Jim Sproch
	@version 0.1a beta
*/

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;

public class UserIO extends KeyAdapter implements MouseListener, FocusListener
{

	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseClicked(MouseEvent e) { if(!AForce.getClicker().isRunning()) AForce.pause(); }

	public void focusGained(FocusEvent e) {}
	public void focusLost(FocusEvent e) { if(AForce.getClicker().isRunning()) AForce.pause(); }

	Ship usership;

	UserIO()
	{
	}

	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	public void keyPressed(KeyEvent e)
	{
	//	char ch = e.getKeyChar();
		int keyCode = e.getKeyCode();

	//	System.out.println("char typed= " + ch);
	//	System.out.println("int typed=" + keyCode);
		if(usership != null && usership.getStatus().getHealth() != 0)
		{
			if(usership.getStatus().getHealth() == 0)
			{
				usership = null;
				System.out.println("dead");
				return;
			}

			if(keyCode == KeyEvent.VK_LEFT) usership.turn(9);
			if(keyCode == KeyEvent.VK_RIGHT) usership.turn(3);
			if(keyCode == KeyEvent.VK_UP) usership.turn(12);
			if(keyCode == KeyEvent.VK_DOWN) usership.turn(6);
			if(keyCode == KeyEvent.VK_SPACE) usership.shoot();
			if(keyCode == KeyEvent.VK_M) usership.placemine();
			if(keyCode == KeyEvent.VK_L) usership.laser();
			if(keyCode == KeyEvent.VK_F) usership.shoot();
			if(keyCode == KeyEvent.VK_X) usership.xshoot();
			if(keyCode == KeyEvent.VK_T) (new Thread(new Runnable(){public void run(){usership.tshoot();}})).start();
			if(keyCode == KeyEvent.VK_C) (new Thread(new Runnable(){public void run(){usership.cshoot();}})).start();
			if(keyCode == KeyEvent.VK_R) usership.reverse();
			if(keyCode == KeyEvent.VK_S) usership.stop();
			if(keyCode == KeyEvent.VK_D) usership.remoteDetonator();
			if(keyCode == KeyEvent.VK_B) usership.timeBomb();
			if(keyCode == KeyEvent.VK_W) usership.warp();
						// For some reason, destroying usership will crash the thread, should fix!
		//	if(keyCode == KeyEvent.VK_D) new Thread(){public void run(){Ship.userShip.destroy();}}.start();
		//	if(keyCode == KeyEvent.VK_D) Ship.userShip.destroy();
			if(keyCode == KeyEvent.VK_A) usership.autopilot();

			if(keyCode == KeyEvent.VK_P) AForce.pause();
			else if(!AForce.getClicker().isRunning()) AForce.pause();

		}

			if(keyCode == KeyEvent.VK_PRINTSCREEN) AForce.getFrame().saveSnapShot();

			if(keyCode == KeyEvent.VK_ESCAPE) Printer.err.println("ERROR 502: No Menu set to the ESC key #UserIO.keyPressed");
	

	}

	public void addship(Ship tempship)
	{
		usership = tempship;
	}

}