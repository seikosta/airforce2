// Dashboard.java
// Jim Sproch
// Created: May 7, 2007
// Modified: March 30, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Dashboard displays the score, lives, messages, etc!
	@author Jim Sproch
	@version 0.1a beta
*/


import java.awt.*;
import javax.swing.*;
import java.awt.image.*; //for double buffering

public class Dashboard
{
	public BufferedImage buffer;
	public Graphics2D graphics;
	Size size = null;
	private boolean update = false;

	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	public Dashboard(Size size)
	{
		this.size = size;
		buffer = new BufferedImage(size.getx(), size.gety(), BufferedImage.TYPE_INT_RGB);
		graphics = buffer.createGraphics();
		update();
	}

	public void requestUpdate()
	{
		this.update = true;
	}

	public void update()
	{
		graphics.drawImage((new ImageIcon(Images.class.getResource("Images/sidebar.png"))).getImage(), 0, 0, null);

		graphics.setColor(Color.gray);
		graphics.setFont(new Font("Java 2D", 2, 30));
		graphics.drawString("Level: "+AForceEnv.getLevel(), 540-525, 350);
		String score;
		if(Ship.userShip != null) score = ""+AForce.field.getScore(Ship.userShip.getTeam());
		else score = "0";
		graphics.drawString(score, 560-525, 400);
		if(false)
		{
			graphics.setColor(Color.red);
			graphics.setFont(new Font("Java 2D", 2, 20));
			graphics.drawString("You are playing in", 530-525, 450);
			graphics.setFont(new Font("Java 2D", 2, 35));
			graphics.drawString("God mode!", 530-525, 500);
		}
		else
		{
			int lives;
			if(Ship.userShip != null) lives = Ship.userShip.getStatus().getLives();
			else lives = 3;
			int spacer = 200/(lives+1);


			graphics.setColor(Color.gray);
			graphics.setFont(new Font("Java 2D", 2, 20));
			graphics.drawString("Lives:", 530-525, 450);

			if(lives < 5)
				for(; lives > 0; lives--)
					graphics.drawImage((new ImageIcon(Images.class.getResource("Images/ship_23x23_2x2_grey-NORTH.png"))).getImage(), spacer*lives-20, 470, null);
				else
				{
					graphics.drawImage((new ImageIcon(Images.class.getResource("Images/ship_23x23_2x2_grey-NORTH.png"))).getImage(), 30, 470, null);
					graphics.setColor(Color.gray);
					graphics.setFont(new Font("Java 2D", 2, 16));
					graphics.drawString("x "+lives, 60, 490);
				}
		}
	}


	public void paint(Graphics g)
	{
		if(update) update();
		g.drawImage(buffer, size.getoffsetx(), size.getoffsety(), null);

		// The rest of this method is devoted to displaying messages
		// There is no need to continue if the game isn't paused (no messages to display)
		if(AForce.getClicker().isRunning()) return;

		// We are at a new game (more important than level)!
		if(!AForce.getFrame().hasFocus())
		{
			grayOut(g);
			g.drawImage((new ImageIcon(Images.class.getResource("Images/keyFocus.png"))).getImage(), 143, 165, null);
			return;
		}

		// We are at a new game (more important than level)!
		if(AForce.getClicker().getRealGameTime() < 5000)
		{
			grayOut(g);
			g.drawImage((new ImageIcon(Images.class.getResource("Images/newGame.png"))).getImage(), 143, 165, null);
			return;
		}

		// We are at a new level!
		if(AForce.getClicker().getRealLevelTime() < 3000)
		{
			grayOut(g);
			g.drawImage((new ImageIcon(Images.class.getResource("Images/newLevel.png"))).getImage(), 143, 165, null);
			return;
		}

		// The game has been paused (no other messages)
		if(true)
		{
			grayOut(g);
			g.drawImage((new ImageIcon(Images.class.getResource("Images/paused.png"))).getImage(), 143, 165, null);
			return;
		}

	}

	// Warning, extremely slow, only possible when no motion on screen
	// TODO Someday: make it faster!
	public void grayOut(Graphics g)
	{
		for(int x = 0; x < 21; x++)
			for(int y = 0; y < 21; y++)
				g.drawImage((new ImageIcon(Images.class.getResource("Images/transBlock.png"))).getImage(), x*25, y*25, null);
	}

}
