// Block.java
// Jim Sproch
// Created: April 29, 2006
// Modified: January 6, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Block is a box which a ship may not pass through
	@author Jim Sproch
	@version 0.1a beta
*/



import java.awt.*;
import javax.swing.*;
import java.util.*;

public class Block extends JComponent implements DisplayableObject
{
	Size mysize;
	Location mylocation;

	int direction;
	int myteam;
	Picture myPicture;
	Status mystatus;

	ArrayList<Boundary> boundaries = new ArrayList<Boundary>();

	// Because this object is Serializable because it extends JComponent
	private static final long serialVersionUID = 7526471155622776147L;



	Block(Picture picture, Size size, Location location)
	{
		mysize = size;
		mylocation = location;
		myPicture = picture;
		direction = 0;

		boundaries.add(new Boundary(mylocation, 0, 0, Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(mylocation, 0, 25, Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(mylocation, 0, 0, Direction.SOUTH, mysize.gety(), this));
		boundaries.add(new Boundary(mylocation, 25, 0, Direction.SOUTH, mysize.gety(), this));

		mystatus = new Status(-1,0);
	}

	@Deprecated
	public boolean isdestroyable()
	{
		return isDestroyable();
	}

	public boolean isDestroyable()
	{
		return false;
	}

	public Size getsize()
	{
		return mysize;
	}

	@Deprecated
	public ArrayList<Boundary> getboundaries()
	{
		return getBoundaries();
	}

	public ArrayList<Boundary> getBoundaries()
	{
		return boundaries;
	}

	public Location getlocation()
	{
		return mylocation;
	}

	@Deprecated
	public void setteam(int team)
	{
		setTeam(team);
	}

	@Deprecated
	public int getteam()
	{
		return getTeam();
	}

	public void setTeam(int team)
	{
		myteam = team;
	}

	public int getTeam()
	{
		return myteam;
	}



	public String toString()
	{
		return "Block @ " + mylocation;
	}


	public void paint(Graphics g)
	{
	//	System.out.println("The block should now have been drawn!!! #Block.paint()");
		g.drawImage(myPicture.getImage(0), mylocation.getx(), mylocation.gety(), this);
	}


	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	public void destroy()
	{
	
	}

	public Status getStatus()
	{
		return mystatus;
	}

	@Deprecated
	public AbstractObject getowner()
	{
		return getOwner();
	}

	@Deprecated
	public Status getstatus()
	{
		return getStatus();
	}

	public AbstractObject getOwner()
	{
		return null;
	}
}