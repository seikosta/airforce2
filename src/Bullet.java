// Bullet.java
// Jim Sproch
// Created: April 29, 2006
// Modified: January 11, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Bullets kill, quite literally!
	@author Jim Sproch
	@version 0.1a beta
*/


import java.awt.*;
import javax.swing.*;
import java.util.*;

public class Bullet extends JComponent implements MovableObject, Weapon
{
	Size mysize;
	Location mylocation;
	int myid;
	int mybullettype;
	int myteam;
	Status mystatus;
	Picture myPicture;

	int mydirection;

	double initialVelocity = 0.14;  // Default: 0.14
	double initialAcceleration = 0.0045; // Default: 0.0045
	int distanceTraveled = 0;

	long birthday = System.currentTimeMillis();

	// Because this object is Serializable because it extends JComponent
	private static final long serialVersionUID = 7526471155622776147L;

	ArrayList<Boundary> boundaries = new ArrayList<Boundary>();

	AForceEnv field;
	AbstractObject myowner;

	public long getBirthday()
	{
		// Used to find the age of a bullet
		return birthday;
	}


	public String toString()
	{
		return "Bullet @ " + mylocation;
	}

	public int getDirection()
	{
		return mydirection;
	}

	Bullet(Size size, Location location, int direction)
	{
		mybullettype = 0;
		mysize = size;
		mylocation = location;
		mydirection = direction;
		mystatus = new Status(1,1);

		boundaries.add(new Boundary(location, mysize.getoffsetx(), mysize.getoffsety(), Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(location, mysize.getoffsetx(), mysize.gety()+mysize.getoffsety(), Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(location, mysize.getoffsetx(), mysize.getoffsety(), Direction.SOUTH, mysize.gety(), this));
		boundaries.add(new Boundary(location, mysize.getx()+mysize.getoffsety(), mysize.getoffsetx(), Direction.SOUTH, mysize.gety(), this));

		// Because bullets spawn in the middle of a level, the clicker will already have elapsed some
		//   time, but there won't be any distance covered, so the bullets will seem to go really fast
		//   So fast, in fact, that you can't even see them!
		distanceTraveled = (int)(getVelocity()*AForce.clicker.getElapsedTime());
	}

	Bullet(Picture picture, Size size, Location location, int direction)
	{
		this(size, location, direction);
		setPicture(picture);
	}

	public boolean isDestroyable()
	{
		return false;
	}

	public void setTeam(int team)
	{
		myteam = team;
	}

	public int getTeam()
	{
		return myteam;
	}


	public Status getStatus()
	{
		return mystatus;
	}


	public Size getsize()
	{
		return mysize;
	}

	public Location getlocation()
	{
		return mylocation;
	}

	public void destroy()
	{
		field.removeObject(this);
	}

	public double getVelocity()
	{
		return initialVelocity+initialAcceleration*((double)AForceEnv.getLevel());
	}

	public void move()
	{
		if(getStatus().getHealth() == 0) return;
		while(1 < AForce.clicker.getElapsedTime()*getVelocity() - distanceTraveled)
		{
			mylocation.move(mydirection);
			field.collisionCheck(this);
			if(getStatus().getHealth() == 0) return;
			distanceTraveled++;
		}
	}


	public int getID()
	{
		return myid;
	}

	public void setField(AForceEnv tempfield)
	{
		field = tempfield;
	}

	public void setOwner(AbstractObject owner)
	{
		myowner = owner;
	}

	public AbstractObject getOwner()
	{
		return myowner;
	}

	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	public Boundary relaventBoundary(int direction)
	{
		if(direction == Direction.NORTH) return boundaries.get(0);
		if(direction == Direction.SOUTH) return boundaries.get(1);
		if(direction == Direction.WEST) return boundaries.get(2);
		if(direction == Direction.EAST) return boundaries.get(3);

		Printer.err.println("ERROR 514: Direction could not be detected! Returning Null! #Ship.relaventboundary()");
		return null;
	}


	public ArrayList<Boundary> getBoundaries()
	{
		return boundaries;
	}

	public void setPicture(Picture picture)
	{
		myPicture = picture;
	}


	public void paint(Graphics g)
	{

		if(g == null || getStatus().getHealth() == 0) return;
		g.drawImage(myPicture.getImage(Direction.NULL), mylocation.getx(), mylocation.gety(), this);
	}
}