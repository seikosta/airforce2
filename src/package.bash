# package.bash
# Jim Sproch
# Created: January 10, 2008
# Modified: January 10, 2008
# Part of the Aforce Port
# Mac < Windows < Linux

rm ../AForce.tar.bz2
rm ../AForce.jar
rm Data/*
rm ScreenShots/*
rm *~
rm *.class
tar cf AForce.tar *.java Maps* Data* SavedGames* Images* *.bash MANIFEST.MF
bzip2 AForce.tar
mv AForce.tar.bz2 ..

javac *.java -source 1.5
jar cmf MANIFEST.MF AForce.jar *.class Maps* Data* SavedGames* Images* *.bash
mv AForce.jar ..

rm *.class
