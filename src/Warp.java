// Warp.java
// Jim Sproch
// Created: May 15, 2007
// Modified: January 6, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Warp is a dummy class so that the Warp Drive can be treated as a weapon!
	@author Jim Sproch
	@version 0.1a beta
*/


import java.util.*;

public class Warp implements AbstractObject
{

	AbstractObject owner;

	// This is a dummy class so that the Warp Drive can be treated as a weapon
	// This is important because of how Commander.WeaponsControl is implemented

	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	Warp(AbstractObject owner)
	{
		this.owner = owner;
	}

	public Location getlocation(){return new Location(0,0);}
	public Size getsize(){return new Size(0,0);}
	public String toString(){return "Instance of Warp Drive by"+owner;}
	@Deprecated
	public AbstractObject getowner(){return getOwner();}
	public AbstractObject getOwner(){return owner;}
	@Deprecated
	public ArrayList<Boundary> getboundaries(){return getBoundaries();}
	public ArrayList<Boundary> getBoundaries(){return new ArrayList<Boundary>();}
	public void destroy(){}
	@Deprecated
	public Status getstatus(){return getStatus();}
	public Status getStatus(){return new Status(0);}
	@Deprecated
	public int getteam(){return getTeam();}
	public int getTeam(){return owner.getTeam();}
	public void setTeam(int team){owner.setTeam(team);}
}