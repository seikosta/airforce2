public class Direction
{
	public static final int NULL = 0;
	public static final int NORTH = 12;
	public static final int WEST = 9;
	public static final int SOUTH = 6;
	public static final int EAST = 3;

	public static String toString(int direction)
	{
		if(direction == NORTH) return "NORTH";
		if(direction == SOUTH) return "SOUTH";
		if(direction == EAST) return "EAST";
		if(direction == WEST) return "WEST";
		if(direction == NULL) return "NULL";
		
		return "ERROR 129: Not a valid direction ("+direction+")! #Direction.toString(int)";
	}

	public static int reverse(int direction)
	{
		switch(direction)
		{
			case NORTH: direction = SOUTH; break;
			case SOUTH: direction = NORTH; break;
			case WEST: direction = EAST; break;
			case EAST: direction = WEST; break;
			default: break;
		}
		return direction;
	}

	public static int toInt(String direction)
	{
		if("NORTH".equals(direction.toUpperCase())) return NORTH;
		if("SOUTH".equals(direction.toUpperCase())) return SOUTH;
		if("EAST".equals(direction.toUpperCase())) return EAST;
		if("WEST".equals(direction.toUpperCase())) return WEST;

		if("NULL".equals(direction.toUpperCase())) return NULL;
		if("ZERO".equals(direction.toUpperCase())) return NULL;
		if("0".equals(direction.toUpperCase())) return NULL;
		if("0/ZERO/NULL".equals(direction.toUpperCase())) return NULL;
		
		Printer.err.println("ERROR 129: Not a valid direction ("+direction+")! returning 0! #Direction.toInt(String)");
		return 0;
	}

	public static void main(String[] args)
	{
		Printer.noexecute();
	}


}