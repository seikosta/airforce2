// Size.java
// Jim Sproch
// Created: April 29, 2006
// Modified: April 29, 2006
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Size defines the size and shape of an object.  Shape has not yet been implemented!
	@author Jim Sproch
	@version 0.1a beta
*/


public class Size
{
	private int myx;
	private int myy;
	private int myoffsetx;
	private int myoffsety;

	public Size(int x, int y)
	{
		myx = x;
		myy = y;
	}

	public Size(int x, int y, int offsetx, int offsety)
	{
		myx = x;
		myy = y;
		myoffsetx = offsetx;
		myoffsety = offsety;
	}

	public int getx()
	{
		return myx;
	}
	public int gety()
	{
		return myy;
	}
	
	public int getoffsetx()
	{
		return myoffsetx;
	}

	public int getoffsety()
	{
		return myoffsety;
	}


	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	public String toString()
	{
		return "Size("+myx+"x"+myy+"_"+myoffsetx+"x"+myoffsety+")";
	}

}