// TimeBomb.java
// Jim Sproch
// Created: March 31, 2007
// Modified: January 10, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	TimeBomb explodes after a specific amount of time!
	@author Jim Sproch
	@version 0.1a beta
*/


import java.awt.*;
import javax.swing.*;
import java.util.*;

public class TimeBomb extends JComponent implements DisplayableObject, Weapon
{
	Size mysize;
	Location mylocation;
	int myid;
	int mybullettype;
	int myteam;
	Status mystatus;
	Picture myPicture;

	// Objects we already hit, so we don't double dip
	ArrayList<AbstractObject> alreadyHit = new ArrayList<AbstractObject>();

	int radius = 50;

	long deathday = AForce.clicker.getElapsedTime()+1500;

	// Because this object is Serializable because it extends JComponent
	private static final long serialVersionUID = 7526471155622776147L;

	ArrayList<Boundary> boundaries = new ArrayList<Boundary>();

	AForceEnv field;
	AbstractObject myowner;


	public String toString()
	{
		return "TimeBomb @ " + mylocation;
	}

	public int getDirection()
	{
		return Direction.NULL;
	}

	TimeBomb(Size size, Location location)
	{
		mybullettype = 0;
		mysize = size;
		mylocation = location;
		mystatus = new Status(10,0);

		boundaries.add(new Boundary(location, mysize.getoffsetx(), mysize.getoffsety(), Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(location, mysize.getoffsetx(), mysize.gety()+mysize.getoffsety(), Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(location, mysize.getoffsetx(), mysize.getoffsety(), Direction.SOUTH, mysize.gety(), this));
		boundaries.add(new Boundary(location, mysize.getx()+mysize.getoffsety(), mysize.getoffsetx(), Direction.SOUTH, mysize.gety(), this));

		myPicture = Images.getPicture("TimeBomb");

	}



	public boolean isDestroyable()
	{
		return false;
	}

	public void setTeam(int team)
	{
		myteam = team;
	}

	public int getTeam()
	{
		return myteam;
	}

	public Status getStatus()
	{
		return mystatus;
	}


	public Size getsize()
	{
		return mysize;
	}

	public Location getlocation()
	{
		return mylocation;
	}
	
	public void setDeathday(long deathday)
	{
		this.deathday = deathday;
	}


	public void destroy()
	{
		// This is a "bomb" that explodes when it is destroyed
		// Thus, a destroy will destroy all other destroyable objects within range

		ArrayList<Boundary> otherBoundaries = AForce.field.getBoundariesH(mylocation.getx()+mysize.getx()/2-radius, mylocation.gety()+mysize.gety()/2-radius, mylocation.getx()+mysize.getx()/2+radius, mylocation.gety()+mysize.gety()/2+radius);
		for(int i = 0; i < otherBoundaries.size(); i++)
		{
			if(alreadyHit.contains(otherBoundaries.get(i).getOwner())) continue;
			alreadyHit.add(otherBoundaries.get(i).getOwner());

			if(otherBoundaries.get(i).getOwner() instanceof MovableObject)
				if(((MovableObject)otherBoundaries.get(i).getOwner()).isDestroyable())
					((MovableObject)otherBoundaries.get(i).getOwner()).getStatus().setHealth(0);
			if(otherBoundaries.get(i).getOwner() instanceof TimeBomb)
				((TimeBomb)otherBoundaries.get(i).getOwner()).setDeathday(0);
			if(otherBoundaries.get(i).getOwner() instanceof Mine)
				((Mine)otherBoundaries.get(i).getOwner()).destroy();
		}
		field.removeObject(this);
	}


	public int getid()
	{
		return myid;
	}

	public void setField(AForceEnv tempfield)
	{
		field = tempfield;
	}


	public void setOwner(AbstractObject owner)
	{
		myowner = owner;
	}

	public AbstractObject getOwner()
	{
		return myowner;
	}

	public static void main(String[] args)
	{
		Printer.noexecute();
	}


	public Boundary relaventBoundary(int direction)
	{
		if(direction == Direction.NORTH) return boundaries.get(0);
		if(direction == Direction.SOUTH) return boundaries.get(1);
		if(direction == Direction.WEST) return boundaries.get(2);
		if(direction == Direction.EAST) return boundaries.get(3);

		Printer.err.println("ERROR 514: Direction could not be detected! Returning Null! #Ship.relaventboundary()");
		return null;
	}

	public ArrayList<Boundary> getBoundaries()
	{
		return boundaries;
	}


	public void paint(Graphics g)
	{
		if(g == null || getStatus().getHealth() == 0) return;

		if(AForce.clicker.getElapsedTime() >= deathday)
		{
			g.setColor(Color.YELLOW);
			// Draw Filled in Yellow Circle to represent explosion
			g.fillOval(mylocation.getx()+mysize.getx()/2-radius, mylocation.gety()+mysize.gety()/2-radius, 2*radius, 2*radius);
			destroy();
		}
		else g.drawImage(myPicture.getImage(Direction.NULL), mylocation.getx(), mylocation.gety(), this);
	}
}