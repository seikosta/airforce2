// Boundary.java
// Jim Sproch
// Created: April 29, 2006
// Modified: January 11, 2008
// Part of the Aforce Port
// Mac < Windows < Linux
// ^^ Be careful, it is amazing how many people read that incorrectly! ^^

/**
	Walls keep the user on the map, and are placed around all edges. They are NOT the green boxes!
	@author Jim Sproch
	@version 0.1a beta
*/

import java.util.*;

public class Boundary implements AbstractObject//, Comparable
{
	int mylength;
	Location mylocation;
	int mydirection;
	AbstractObject myowner;
	int xoffset, yoffset;

	Boundary(Location location, int xoffset, int yoffset, int direction, int length, AbstractObject owner)
	{
		myowner = owner;
		mydirection = direction;
		mylocation = location;
		mylength = length;
		this.xoffset = xoffset;
		this.yoffset = yoffset;
	}

/*	public int compareTo(Object obj)
	{
		if(mydirection == Direction.NORTH || mydirection == Direction.SOUTH)
			return mylocation.getx() - ((Boundary)obj).getlocation().getx();
		if(mydirection == Direction.EAST || mydirection == Direction.WEST)
			return mylocation.gety() - ((Boundary)obj).getlocation().gety();
		return 1;
	}

	public boolean equals(Object obj)
	{
		if(mydirection == Direction.NORTH || mydirection == Direction.SOUTH)
			if(mylocation.getx() == ((Boundary)obj).getlocation().getx())
				return true;
		if(mydirection == Direction.EAST || mydirection == Direction.WEST)
			if(mylocation.gety() == ((Boundary)obj).getlocation().gety())
				return true;
		return false;
	}
*/

	public Location getlocation()
	{
		return mylocation;
	}

	public void destroy()
	{
	
	}

	public Status getStatus()
	{
		return null;
	}



	public String toString()
	{
		return "Boundary @ ("+getx1()+", "+gety1()+") extends "+mylength+" pixels and faces "+Direction.toString(mydirection)+" owned by: "+myowner;
	}


	public int getTeam()
	{
		return myowner.getTeam();
	}

	public void setTeam(int team)
	{
		myowner.setTeam(team);
	}
	public AbstractObject getOwner()
	{
		return myowner;
	}

	public void setOwner(AbstractObject owner)
	{
		myowner = owner;
	}

	public int getDirection()
	{
		return mydirection;
	}

	public int getlength()
	{
		return mylength;
	}


	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	public int getx1()
	{
		return mylocation.getx()+xoffset;
	}

	public int getx2()
	{
		if(mydirection == Direction.NORTH || mydirection == Direction.SOUTH)
			return getx1();
		if(mydirection == Direction.EAST || mydirection == Direction.WEST)
			return getx1()+mylength;
		
		Printer.err.println("ERROR 734: Unhandled direction! #Boundary.getx2()");
		return getx1();
	}

	public int gety1()
	{
		return mylocation.gety()+yoffset;
	}

	public int gety2()
	{
		if(mydirection == Direction.NORTH || mydirection == Direction.SOUTH)
			return gety1()+mylength;
		if(mydirection == Direction.EAST || mydirection == Direction.WEST)
			return gety1();
		
		Printer.err.println("ERROR 735: Unhandled direction! #Boundary.gety2()");
		return getx1();
	}


	public int vdistancefrom(Boundary boundary)
	{
		return Math.abs(gety1() - boundary.gety1());
	}




	public ArrayList<Boundary> getBoundaries()
	{
		ArrayList<Boundary> output = new ArrayList<Boundary>();
		output.add(this);
		return output;
	}

	public Size getsize()
	{
		if(mydirection == Direction.NORTH || mydirection == Direction.SOUTH)
			return new Size(0,mylength);
		if(mydirection == Direction.EAST || mydirection == Direction.WEST)
			return new Size(mylength,0);
		Printer.out.println("ERROR 887: Direction not recognized #Boundary.getsize()");
		return null;
	}

}
