// Clicker.java
// Jim Sproch
// Created: April 29, 2006
// Modified: January 10, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Clicker is now just a simple game clock!
	@author Jim Sproch
	@version 0.1a beta
*/

public class Clicker implements Runnable
{
	// The game has a control thread
	private boolean hasControl = false;

	// Time stamps for the executable
	long exe_starttime = System.currentTimeMillis(); // Time stamp the executable was started
	long exe_pausetime = 0;  //how long the executable has been paused

	// Time stamps for the game ("Game>New Game")
	long game_start = System.currentTimeMillis(); // Time stamp the game was started
	long game_pausetime = 0; // how long the game has been paused

	// Time stamps for the level
	long level_starttime = System.currentTimeMillis(); // Time stamp the level was started
	long level_pausetime = 0;  // how long the level has been paused

	// Time stamp that the most recent "pause game" was activated
	// Set to zero if the game is not paused
	long pause_startTime = System.currentTimeMillis();

	// Jumpstarts the clicker, no matter what it takes
	public void start()
	{
		if(isRunning()) Printer.wrn.println("WARNING 259: Clicker has already been started #Clicker.start()");
		else go();

		if(hasControl) return;
		else hasControl = true;

		Thread thread = new Thread(this);
		AForce.getThreads().add(thread);
		thread.start();
	}

	public void run()
	{
		// Effectively "while(true)"
		while(hasControl)
		{
			try{ Thread.sleep(10); }catch(Exception e){}
			if(pause_startTime == 0)
			{
				AForce.field.click();
			}
		}
	}

	// Pauses (Stops) the game
	public void stop()
	{
		pause_startTime = System.currentTimeMillis();
		return;
	}

	// UnPauses the game (Gets the clicker clicking)
	private void go()
	{
		long recentPausedTime = System.currentTimeMillis() - pause_startTime;

		exe_pausetime += recentPausedTime;  //how long the executable has been paused
		game_pausetime += recentPausedTime; // how long the game has been paused
		level_pausetime += recentPausedTime;  // how long the level has been paused


		// Set to zero if the game is not paused
		pause_startTime = 0;
		return;
	}

	// Checks if the clicker is clicking or if it is paused
	public boolean isRunning()
	{
		if(pause_startTime!= 0) return false;
		return true;
	}

	// Resets all tiems for this level (ie. new level)
	public void resetLevelTimes()
	{
		// Time stamps for the level
		level_starttime = System.currentTimeMillis(); // Time stamp the level was started
		level_pausetime = 0;  // how long the level has been paused
	}

	public long getRealLevelTime()
	{
		return System.currentTimeMillis()-level_starttime;
	}

	// Resets all times for this game including level times (ie. new game)
	public void resetGameTimes()
	{
		game_start = System.currentTimeMillis(); // Time stamp the game was started
		game_pausetime = 0; // how long the game has been paused

		resetLevelTimes(); // Reset Level Times
	}

	public long getRealGameTime()
	{
		return System.currentTimeMillis()-game_start;
	}

	// Gets the total executable run time minus the executable's paused time
	public long getElapsedTime()
	{
		if(pause_startTime != 0)
			return (System.currentTimeMillis() - exe_starttime) + (pause_startTime - System.currentTimeMillis()) - exe_pausetime;
		return (System.currentTimeMillis() - exe_starttime) - exe_pausetime;
	}

	// Gets the total executable's run time (including paused time)
	public long getTotalTime()
	{
		return System.currentTimeMillis() - exe_starttime;
	}


	public void main(String[] args)
	{
		Printer.noexecute();
	}

}
