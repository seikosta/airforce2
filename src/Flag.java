// Flag.java
// Jim Sproch
// Created: March 31, 2007
// Modified: January 11, 2008
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	Flags are used in the Capture-the-Flag Games
	@author Jim Sproch
	@version 0.1a beta
*/



import java.awt.*;
import javax.swing.*;
import java.util.*;

public class Flag extends JComponent implements DisplayableObject
{
	Size mysize;
	Location mylocation;

	int myteam;
	Picture myPicture;
	Status mystatus;

	ArrayList<Boundary> boundaries = new ArrayList<Boundary>();

	// Because this object is Serializable because it extends JComponent
	private static final long serialVersionUID = 7526471155622776147L;



	Flag(Picture picture, Location location)
	{
		mysize = new Size(16, 26, 5, 0);
		mylocation = location;
		myPicture = picture;

		boundaries.add(new Boundary(mylocation, 0, 0, Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(mylocation, 0, 25, Direction.EAST, mysize.getx(), this));
		boundaries.add(new Boundary(mylocation, 0, 0, Direction.SOUTH, mysize.gety(), this));
		boundaries.add(new Boundary(mylocation, 25, 0, Direction.SOUTH, mysize.gety(), this));

		mystatus = new Status(-1,0);
	}

	public boolean isDestroyable()
	{
		return false;
	}

	public Size getsize()
	{
		return mysize;
	}


	public ArrayList<Boundary> getBoundaries()
	{
		return boundaries;
	}

	public Location getlocation()
	{
		return getlocation();
	}


	public void setTeam(int team)
	{
		myteam = team;
	}

	public int getTeam()
	{
		return myteam;
	}



	public String toString()
	{
		return "Flag @ " + mylocation;
	}


	public void paint(Graphics g)
	{
	//	System.out.println("The block should now have been drawn!!! #Block.paint()");
		g.drawImage(myPicture.getImage(0), mylocation.getx(), mylocation.gety(), this);
	}


	public static void main(String[] args)
	{
		Printer.noexecute();
	}

	public void destroy()
	{
	
	}

	public Status getStatus()
	{
		return mystatus;
	}


	public AbstractObject getOwner()
	{
		return null;
	}
}