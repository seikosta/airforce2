// RecordableEvent.java
// Jim Sproch
// Created: May 7, 2007
// Modified: May 7, 2007
// Part of the Aforce Port
// Mac < Windows < Linux

/**
	RecordableEvent is created when something worthy of recording happens!
	@author Jim Sproch
	@version 0.1a beta
*/

public class RecordableEvent
{

	public boolean isActionItem()
	{
		return false;
	}

	public boolean isSystemItem()
	{
		return false;
	}

	public static void main(String[] args)
	{
		Printer.noexecute();
	}

}
